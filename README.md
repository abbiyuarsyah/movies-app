# Movies App
A simple CRUD app

# Installing
```
1. git clone
2. flutter get packages
```

# Description
```
The project is already tried by Pixel XL Emulator Android, and it runs well. 
If there's something wrong when you run the project, please send me a text via email abbiyuarsyah@gmail.com :)
```

# Screenshot
<img height="480" src="/uploads/5cdcb4d0d95b588a75996f99926c5296/movie_list.png">
<img height="480" src="/uploads/b057cb0d38c51f47e140cde4ee220e30/movie_new.png">
<img height="480" src="/uploads/d9f9187c163d02d185e9e3eb31b116b0/movie_edit.png">
