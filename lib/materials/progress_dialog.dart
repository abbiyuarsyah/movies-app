import 'package:flutter/material.dart';

final progressDialog = ProgressDialog();

class ProgressDialog {
  progressDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  dimissProgressDialog(BuildContext context) {
    Navigator.pop(context);
  }
}
