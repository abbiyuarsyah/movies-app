import 'dart:convert';

import 'package:movies_app/data/constants/pref_key.dart';
import 'package:movies_app/data/models/categories.dart';
import 'package:movies_app/helpers/dummy_helpert.dart';
import 'package:movies_app/helpers/pref_helper.dart';

import '../models/movie.dart';

class MovieRepository {
  Future<List<Movie>> fetchMovies() async {
    return Future.delayed(
      Duration(seconds: 1),
      () async {
        List<Movie> movies = [];
        final list = await PrefHelper().getString(PrefKey.movies);

        if (list != "") {
          movies = (json.decode(list) as List<dynamic>)
              .map<Movie>((item) => Movie.fromJson(item))
              .toList();
        }

        if (movies.length > 0) {
          return movies;
        }

        return DummyHelper().initMovies();
      },
    );
  }

  Future<Categories> fetchCategories() {
    return Future.delayed(
      Duration(seconds: 1),
      () async {
        return DummyHelper().initCategories();
      },
    );
  }
}
