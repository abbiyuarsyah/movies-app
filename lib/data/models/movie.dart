import 'package:flutter/material.dart';

class Movie {
  var id;
  var title;
  var director;
  var summary;
  var tags;

  Movie(
      {@required this.id,
      @required this.title,
      @required this.director,
      @required this.summary,
      @required this.tags});

  factory Movie.fromJson(Map<String, dynamic> jsonData) {
    return Movie(
      id: jsonData['id'],
      title: jsonData['title'],
      director: jsonData['director'],
      summary: jsonData['summary'],
      tags: jsonData['tags'],
    );
  }

  static Map<String, dynamic> toMap(Movie movie) => {
        'id': movie.id,
        'title': movie.title,
        'director': movie.director,
        'summary': movie.summary,
        'tags': movie.tags,
      };
}
