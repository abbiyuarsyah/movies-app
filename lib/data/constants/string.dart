class StringText {
  static const movies = "Movies";
  static const new_ = "New";
  static const updateMovie = "Update Movie";
  static const delete = "Delete";
  static const update = "Update";
  static const title = "Title";
  static const director = "Director";
  static const summary = "Summary";
  static const action = "Action";
  static const newMovie = "New Movie";
  static const save = "Save";
  static const tags = "Tags";
  static const selectTags = "Please select tags..";
  static const information = "Information";
  static const updateMovieFailed = "Update movie is failed";
  static const somethingIsWrong = "Oopss.. something is wrong";
  static const fillAllForms = "Please fill in all the required fields";
  static const movieDeleted = "Movie has been deleted";
  static const movieUpdated = "Movie has been updated";
  static const movieAdded = "New movie has been added";
  static const deleteMovieFailed = "Delete movie is failed";
  static const addMovieFailed = "Add movie is failed";
}
