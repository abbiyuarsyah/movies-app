// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_new_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MovieNewStore on _MovieNewStore, Store {
  Computed<StoreState>? _$stateComputed;

  @override
  StoreState get state => (_$stateComputed ??=
          Computed<StoreState>(() => super.state, name: '_MovieNewStore.state'))
      .value;
  Computed<StoreState>? _$categoriesStateComputed;

  @override
  StoreState get categoriesState => (_$categoriesStateComputed ??=
          Computed<StoreState>(() => super.categoriesState,
              name: '_MovieNewStore.categoriesState'))
      .value;

  final _$_addMovieFutureAtom = Atom(name: '_MovieNewStore._addMovieFuture');

  @override
  ObservableFuture<String>? get _addMovieFuture {
    _$_addMovieFutureAtom.reportRead();
    return super._addMovieFuture;
  }

  @override
  set _addMovieFuture(ObservableFuture<String>? value) {
    _$_addMovieFutureAtom.reportWrite(value, super._addMovieFuture, () {
      super._addMovieFuture = value;
    });
  }

  final _$_categoriesFutureAtom =
      Atom(name: '_MovieNewStore._categoriesFuture');

  @override
  ObservableFuture<Categories>? get _categoriesFuture {
    _$_categoriesFutureAtom.reportRead();
    return super._categoriesFuture;
  }

  @override
  set _categoriesFuture(ObservableFuture<Categories>? value) {
    _$_categoriesFutureAtom.reportWrite(value, super._categoriesFuture, () {
      super._categoriesFuture = value;
    });
  }

  final _$categoriesAtom = Atom(name: '_MovieNewStore.categories');

  @override
  Categories? get categories {
    _$categoriesAtom.reportRead();
    return super.categories;
  }

  @override
  set categories(Categories? value) {
    _$categoriesAtom.reportWrite(value, super.categories, () {
      super.categories = value;
    });
  }

  final _$successMessageAtom = Atom(name: '_MovieNewStore.successMessage');

  @override
  String? get successMessage {
    _$successMessageAtom.reportRead();
    return super.successMessage;
  }

  @override
  set successMessage(String? value) {
    _$successMessageAtom.reportWrite(value, super.successMessage, () {
      super.successMessage = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_MovieNewStore.errorMessage');

  @override
  String? get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String? value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$addMovieAsyncAction = AsyncAction('_MovieNewStore.addMovie');

  @override
  Future<dynamic> addMovie(
      String title, String director, String summary, String tags) {
    return _$addMovieAsyncAction
        .run(() => super.addMovie(title, director, summary, tags));
  }

  final _$fetchCategorisAsyncAction =
      AsyncAction('_MovieNewStore.fetchCategoris');

  @override
  Future<dynamic> fetchCategoris() {
    return _$fetchCategorisAsyncAction.run(() => super.fetchCategoris());
  }

  @override
  String toString() {
    return '''
categories: ${categories},
successMessage: ${successMessage},
errorMessage: ${errorMessage},
state: ${state},
categoriesState: ${categoriesState}
    ''';
  }
}
