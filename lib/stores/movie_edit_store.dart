import 'dart:convert';

import 'package:mobx/mobx.dart';
import 'package:movies_app/data/constants/pref_key.dart';
import 'package:movies_app/data/constants/store_stare.dart';
import 'package:movies_app/data/constants/string.dart';
import 'package:movies_app/data/models/categories.dart';
import 'package:movies_app/data/models/movie.dart';
import 'package:movies_app/data/repositories/movie_repository.dart';
import 'package:movies_app/helpers/pref_helper.dart';
part 'movie_edit_store.g.dart';

class MovieEditStore extends _MovieEditStore with _$MovieEditStore {}

abstract class _MovieEditStore with Store {
  MovieRepository? _movieRepository = MovieRepository();

  @observable
  ObservableFuture<String>? _updateMovieFuture;

  @observable
  ObservableFuture<Categories>? _categoriesFuture;

  @observable
  Categories? categories;

  @observable
  String? successMessage;

  @observable
  String? errorMessage;

  @computed
  StoreState get updatetate {
    if (_updateMovieFuture == null &&
        _updateMovieFuture?.status == FutureStatus.rejected) {
      return StoreState.initial;
    }
    return _updateMovieFuture?.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @computed
  StoreState get categoriesState {
    if ((_categoriesFuture?.value?.categories.length ?? 0) > 0 &&
        _categoriesFuture?.status == FutureStatus.rejected) {
      return StoreState.initial;
    }
    return _categoriesFuture?.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @action
  Future updateMovie(String title, String director, String summary, String tags,
      int id) async {
    try {
      final validate = await movieValidation(title, director, summary, tags);
      if (validate) {
        List<Movie> movieResponse = [];
        movieResponse = await _movieRepository!.fetchMovies();

        var size = movieResponse.length + 1;
        if (size > 1) {
          final newData = Movie(
              id: id, // Key
              title: title,
              director: director,
              summary: summary,
              tags: tags);

          final result = await updateDataMovies(movieResponse, newData);
          if (result.isNotEmpty) {
            PrefHelper().setString(PrefKey.movies, result);
            _updateMovieFuture = ObservableFuture(
              Future.value(StringText.movieUpdated),
            );

            successMessage = await _updateMovieFuture;
          } else {
            errorMessage = StringText.updateMovieFailed;
          }
        } else {
          errorMessage = StringText.somethingIsWrong;
        }
      } else {
        errorMessage = StringText.fillAllForms;
      }
    } on Exception catch (error) {
      errorMessage = error.toString();
    }
  }

  @action
  Future deleteMovie(int id) async {
    try {
      List<Movie> movieResponse = [];

      movieResponse = await _movieRepository!.fetchMovies();
      if (movieResponse.length > 1) {
        final result = await deleteMovies(movieResponse, id);
        if (result.isNotEmpty) {
          PrefHelper().setString(PrefKey.movies, result);
          _updateMovieFuture = ObservableFuture(
            Future.value(StringText.movieDeleted),
          );

          successMessage = await _updateMovieFuture;
        } else {
          errorMessage = StringText.deleteMovieFailed;
        }
      } else {
        errorMessage = StringText.somethingIsWrong;
      }
    } on Exception catch (error) {
      errorMessage = error.toString();
    }
  }

  @action
  Future fetchCategories() async {
    try {
      _categoriesFuture = ObservableFuture(
        _movieRepository!.fetchCategories(),
      );

      categories = await _categoriesFuture;
    } on Exception catch (error) {
      errorMessage = error.toString();
    }
  }

  Future<bool> movieValidation(
      String title, String director, String summary, String tags) async {
    if (title.isEmpty || director.isEmpty || summary.isEmpty || tags.isEmpty) {
      return false;
    }
    return true;
  }

  Future<String> updateDataMovies(
      List<Movie> movieResponse, Movie? newData) async {
    if (newData == null) {
      return "";
    }

    try {
      for (int i = 0; i < movieResponse.length; i++) {
        if (movieResponse[i].id == newData.id) {
          movieResponse[i] = newData;
        }
      }

      final jsonEncoded = json.encode(
        movieResponse
            .map<Map<String, dynamic>>((movie) => Movie.toMap(movie))
            .toList(),
      );

      return jsonEncoded;
    } on Exception {
      return "";
    }
  }

  Future<String> deleteMovies(List<Movie> movieResponse, int? id) async {
    if (id == null) {
      return "";
    }

    try {
      for (int i = 0; i < movieResponse.length; i++) {
        if (movieResponse[i].id == id) {
          movieResponse.removeWhere((item) => item.id == id);
          final jsonEncoded = json.encode(
            movieResponse
                .map<Map<String, dynamic>>((movie) => Movie.toMap(movie))
                .toList(),
          );
          return jsonEncoded;
        }
      }
      return "";
    } on Exception {
      return "";
    }
  }
}
