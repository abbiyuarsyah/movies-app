import 'package:mobx/mobx.dart';
import 'package:movies_app/data/constants/store_stare.dart';
import 'package:movies_app/data/models/movie.dart';
import 'package:movies_app/data/repositories/movie_repository.dart';
part 'movie_list_store.g.dart';

class MovieListStore extends _MovieListStore with _$MovieListStore {}

abstract class _MovieListStore with Store {
  MovieRepository? _movieRepository = MovieRepository();

  @observable
  ObservableFuture<List<Movie>>? _movieListFuture;

  @observable
  List<Movie>? movies;

  @observable
  String? errorMessage;

  @computed
  StoreState get state {
    if (_movieListFuture?.value?.length == 0 &&
        _movieListFuture?.status == FutureStatus.rejected) {
      return StoreState.initial;
    }
    return _movieListFuture?.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @action
  Future getMovies() async {
    try {
      errorMessage = null;

      _movieListFuture = ObservableFuture(_movieRepository!.fetchMovies());
      movies = await _movieListFuture!;
    } on Exception catch (error) {
      errorMessage = error.toString();
    }
  }
}
