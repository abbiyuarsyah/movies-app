// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_list_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MovieListStore on _MovieListStore, Store {
  Computed<StoreState>? _$stateComputed;

  @override
  StoreState get state =>
      (_$stateComputed ??= Computed<StoreState>(() => super.state,
              name: '_MovieListStore.state'))
          .value;

  final _$_movieListFutureAtom = Atom(name: '_MovieListStore._movieListFuture');

  @override
  ObservableFuture<List<Movie>>? get _movieListFuture {
    _$_movieListFutureAtom.reportRead();
    return super._movieListFuture;
  }

  @override
  set _movieListFuture(ObservableFuture<List<Movie>>? value) {
    _$_movieListFutureAtom.reportWrite(value, super._movieListFuture, () {
      super._movieListFuture = value;
    });
  }

  final _$moviesAtom = Atom(name: '_MovieListStore.movies');

  @override
  List<Movie>? get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(List<Movie>? value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_MovieListStore.errorMessage');

  @override
  String? get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String? value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$getMoviesAsyncAction = AsyncAction('_MovieListStore.getMovies');

  @override
  Future<dynamic> getMovies() {
    return _$getMoviesAsyncAction.run(() => super.getMovies());
  }

  @override
  String toString() {
    return '''
movies: ${movies},
errorMessage: ${errorMessage},
state: ${state}
    ''';
  }
}
