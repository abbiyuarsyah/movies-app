// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_edit_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MovieEditStore on _MovieEditStore, Store {
  Computed<StoreState>? _$updatetateComputed;

  @override
  StoreState get updatetate =>
      (_$updatetateComputed ??= Computed<StoreState>(() => super.updatetate,
              name: '_MovieEditStore.updatetate'))
          .value;
  Computed<StoreState>? _$categoriesStateComputed;

  @override
  StoreState get categoriesState => (_$categoriesStateComputed ??=
          Computed<StoreState>(() => super.categoriesState,
              name: '_MovieEditStore.categoriesState'))
      .value;

  final _$_updateMovieFutureAtom =
      Atom(name: '_MovieEditStore._updateMovieFuture');

  @override
  ObservableFuture<String>? get _updateMovieFuture {
    _$_updateMovieFutureAtom.reportRead();
    return super._updateMovieFuture;
  }

  @override
  set _updateMovieFuture(ObservableFuture<String>? value) {
    _$_updateMovieFutureAtom.reportWrite(value, super._updateMovieFuture, () {
      super._updateMovieFuture = value;
    });
  }

  final _$_categoriesFutureAtom =
      Atom(name: '_MovieEditStore._categoriesFuture');

  @override
  ObservableFuture<Categories>? get _categoriesFuture {
    _$_categoriesFutureAtom.reportRead();
    return super._categoriesFuture;
  }

  @override
  set _categoriesFuture(ObservableFuture<Categories>? value) {
    _$_categoriesFutureAtom.reportWrite(value, super._categoriesFuture, () {
      super._categoriesFuture = value;
    });
  }

  final _$categoriesAtom = Atom(name: '_MovieEditStore.categories');

  @override
  Categories? get categories {
    _$categoriesAtom.reportRead();
    return super.categories;
  }

  @override
  set categories(Categories? value) {
    _$categoriesAtom.reportWrite(value, super.categories, () {
      super.categories = value;
    });
  }

  final _$successMessageAtom = Atom(name: '_MovieEditStore.successMessage');

  @override
  String? get successMessage {
    _$successMessageAtom.reportRead();
    return super.successMessage;
  }

  @override
  set successMessage(String? value) {
    _$successMessageAtom.reportWrite(value, super.successMessage, () {
      super.successMessage = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_MovieEditStore.errorMessage');

  @override
  String? get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String? value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$updateMovieAsyncAction = AsyncAction('_MovieEditStore.updateMovie');

  @override
  Future<dynamic> updateMovie(
      String title, String director, String summary, String tags, int id) {
    return _$updateMovieAsyncAction
        .run(() => super.updateMovie(title, director, summary, tags, id));
  }

  final _$deleteMovieAsyncAction = AsyncAction('_MovieEditStore.deleteMovie');

  @override
  Future<dynamic> deleteMovie(int id) {
    return _$deleteMovieAsyncAction.run(() => super.deleteMovie(id));
  }

  final _$fetchCategoriesAsyncAction =
      AsyncAction('_MovieEditStore.fetchCategories');

  @override
  Future<dynamic> fetchCategories() {
    return _$fetchCategoriesAsyncAction.run(() => super.fetchCategories());
  }

  @override
  String toString() {
    return '''
categories: ${categories},
successMessage: ${successMessage},
errorMessage: ${errorMessage},
updatetate: ${updatetate},
categoriesState: ${categoriesState}
    ''';
  }
}
