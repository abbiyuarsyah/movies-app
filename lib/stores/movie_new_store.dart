import 'dart:convert';

import 'package:mobx/mobx.dart';
import 'package:movies_app/data/constants/pref_key.dart';
import 'package:movies_app/data/constants/store_stare.dart';
import 'package:movies_app/data/constants/string.dart';
import 'package:movies_app/data/models/categories.dart';
import 'package:movies_app/data/models/movie.dart';
import 'package:movies_app/data/repositories/movie_repository.dart';
import 'package:movies_app/helpers/pref_helper.dart';
part 'movie_new_store.g.dart';

class MovieNewStore extends _MovieNewStore with _$MovieNewStore {}

abstract class _MovieNewStore with Store {
  MovieRepository? _movieRepository = MovieRepository();

  @observable
  ObservableFuture<String>? _addMovieFuture;

  @observable
  ObservableFuture<Categories>? _categoriesFuture;

  @observable
  Categories? categories;

  @observable
  String? successMessage;

  @observable
  String? errorMessage;

  @computed
  StoreState get state {
    if (_addMovieFuture == null &&
        _addMovieFuture?.status == FutureStatus.rejected) {
      return StoreState.initial;
    }
    return _addMovieFuture?.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @computed
  StoreState get categoriesState {
    if ((_categoriesFuture?.value?.categories.length ?? 0) > 0 &&
        _categoriesFuture?.status == FutureStatus.rejected) {
      return StoreState.initial;
    }
    return _categoriesFuture?.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @action
  Future addMovie(
      String title, String director, String summary, String tags) async {
    try {
      final validate = await addMovieValidation(title, director, summary, tags);
      if (validate) {
        List<Movie> movieResponse = [];
        movieResponse = await _movieRepository!.fetchMovies();
        var size = movieResponse.length + 1;

        if (size > 1) {
          final newData = Movie(
              id: size++, // Key Increment
              title: title,
              director: director,
              summary: summary,
              tags: tags);

          final result = await saveDataMovies(movieResponse, newData);
          if (result.isNotEmpty) {
            PrefHelper().setString(PrefKey.movies, result);

            _addMovieFuture = ObservableFuture(
              Future.value(StringText.movieAdded),
            );

            successMessage = await _addMovieFuture;
          } else {
            errorMessage = StringText.addMovieFailed;
          }
        } else {
          errorMessage = StringText.somethingIsWrong;
        }
      } else {
        errorMessage = StringText.fillAllForms;
      }
    } on Exception catch (error) {
      errorMessage = error.toString();
    }
  }

  @action
  Future fetchCategoris() async {
    try {
      _categoriesFuture = ObservableFuture(
        _movieRepository!.fetchCategories(),
      );
      categories = await _categoriesFuture;
      print(categories?.categories.length);
    } on Exception catch (error) {
      errorMessage = error.toString();
    }
  }

  Future<String> saveDataMovies(
      List<Movie> movieResponse, Movie? newData) async {
    if (newData == null) {
      return "";
    }

    try {
      movieResponse.add(newData);
      final jsonEncoded = json.encode(
        movieResponse
            .map<Map<String, dynamic>>((music) => Movie.toMap(music))
            .toList(),
      );
      return jsonEncoded;
    } on Exception {
      return "";
    }
  }

  Future<bool> addMovieValidation(
      String title, String director, String summary, String tags) async {
    if (title.isEmpty || director.isEmpty || summary.isEmpty || tags.isEmpty) {
      return false;
    }
    return true;
  }
}
