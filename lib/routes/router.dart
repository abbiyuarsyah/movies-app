import 'package:auto_route/auto_route.dart';
import 'package:movies_app/views/movie_edit_page.dart';
import 'package:movies_app/views/movie_new_page.dart';
import 'package:movies_app/views/movie_list_page.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(page: MovieListPage, initial: true),
    AutoRoute(page: MovieEditPage),
    AutoRoute(page: MovieNewPage)
  ],
)
class $AppRouter {}
