// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;

import '../data/models/movie.dart' as _i6;
import '../views/movie_edit_page.dart' as _i4;
import '../views/movie_list_page.dart' as _i3;
import '../views/movie_new_page.dart' as _i5;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    MovieListPageRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i3.MovieListPage();
        }),
    MovieEditPageRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<MovieEditPageRouteArgs>(
              orElse: () => const MovieEditPageRouteArgs());
          return _i4.MovieEditPage(movie: args.movie);
        }),
    MovieNewPageRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i5.MovieNewPage();
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(MovieListPageRoute.name, path: '/'),
        _i1.RouteConfig(MovieEditPageRoute.name, path: '/movie-edit-page'),
        _i1.RouteConfig(MovieNewPageRoute.name, path: '/movie-new-page')
      ];
}

class MovieListPageRoute extends _i1.PageRouteInfo {
  const MovieListPageRoute() : super(name, path: '/');

  static const String name = 'MovieListPageRoute';
}

class MovieEditPageRoute extends _i1.PageRouteInfo<MovieEditPageRouteArgs> {
  MovieEditPageRoute({_i6.Movie? movie})
      : super(name,
            path: '/movie-edit-page',
            args: MovieEditPageRouteArgs(movie: movie));

  static const String name = 'MovieEditPageRoute';
}

class MovieEditPageRouteArgs {
  const MovieEditPageRouteArgs({this.movie});

  final _i6.Movie? movie;
}

class MovieNewPageRoute extends _i1.PageRouteInfo {
  const MovieNewPageRoute() : super(name, path: '/movie-new-page');

  static const String name = 'MovieNewPageRoute';
}
