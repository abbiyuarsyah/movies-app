import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:movies_app/data/constants/store_stare.dart';
import 'package:movies_app/data/constants/string.dart';
import 'package:movies_app/materials/bottom_sheet.dart';
import 'package:movies_app/materials/progress_dialog.dart';
import 'package:movies_app/stores/movie_new_store.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

class MovieNewPage extends StatefulWidget {
  @override
  _MovieNewPageState createState() => _MovieNewPageState();
}

class _MovieNewPageState extends State<MovieNewPage> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  TextEditingController _titleController = TextEditingController();
  TextEditingController _directorController = TextEditingController();
  TextEditingController _summaryController = TextEditingController();
  FocusNode _titleFocusNode = FocusNode();
  FocusNode _directorFocusNode = FocusNode();
  FocusNode _summaryFocusNode = FocusNode();
  MovieNewStore? _movieNewStore;
  List<ReactionDisposer>? _disposers;
  String _currentSelectedValue = StringText.action;
  List<String>? selectedValues;

  @override
  void initState() {
    _movieNewStore ??= MovieNewStore();
    _disposers ??= [
      reaction((_) => _movieNewStore?.successMessage, (message) {
        progressDialog.dimissProgressDialog(context);
        showModalBottomSheet(
          enableDrag: false,
          isDismissible: false,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (context) => BottomSheetMaterial()
              .bottomSheetDialog(message.toString(), context, () {
            Navigator.pop(context);
            Navigator.pop(context);
          }),
        );
      }),
      autorun((_) => {
            if (_movieNewStore?.errorMessage != null)
              {
                progressDialog.dimissProgressDialog(context),
                showModalBottomSheet(
                  enableDrag: false,
                  isDismissible: false,
                  backgroundColor: Colors.transparent,
                  context: context,
                  builder: (context) => BottomSheetMaterial().bottomSheetDialog(
                      _movieNewStore?.errorMessage?.toString() ?? "", context,
                      () {
                    Navigator.pop(context);
                  }),
                )
              }
          }),
    ];

    _movieNewStore?.fetchCategoris();
    super.initState();
  }

  @override
  void dispose() {
    _disposers?.forEach((d) => d());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldMessengerKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          StringText.newMovie,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => {
              progressDialog.progressDialog(context),
              _movieNewStore?.addMovie(
                  _titleController.text,
                  _directorController.text,
                  _summaryController.text,
                  (selectedValues == null) ? "" : selectedValues!.join(", ")),
            },
            child: Container(
              margin: EdgeInsets.only(right: 16),
              child: Text(
                StringText.save,
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 24),
          child: Column(
            children: [
              _textField(
                  _titleController, _titleFocusNode, StringText.title, false),
              _textField(_directorController, _directorFocusNode,
                  StringText.director, false),
              Observer(builder: (_) {
                switch (_movieNewStore!.categoriesState) {
                  case StoreState.initial:
                    return _dropDownTags();
                  case StoreState.loading:
                    return Center(child: CircularProgressIndicator());
                  case StoreState.loaded:
                    return _dropDownTags();
                }
              }),
              _textField(_summaryController, _summaryFocusNode,
                  StringText.summary, false),
            ],
          ),
        ),
      ),
    );
  }

  Widget _textField(TextEditingController textEditingController,
      FocusNode? focusNode, String labelText, bool readOnly) {
    return Container(
      margin: EdgeInsets.only(left: 24, right: 24, bottom: 24),
      child: TextFormField(
        readOnly: readOnly,
        focusNode: focusNode,
        controller: textEditingController,
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        decoration: InputDecoration(
          alignLabelWithHint: true,
          labelStyle: TextStyle(fontSize: 14, color: Colors.grey),
          labelText: labelText,
          filled: true,
          fillColor: Colors.white,
          enabledBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(8.0),
            borderSide: new BorderSide(
              color: Colors.grey,
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(8.0),
            borderSide: new BorderSide(
              color: Colors.grey,
            ),
          ),
        ),
      ),
    );
  }

  Widget _dropDownTags() {
    return GestureDetector(
      onTap: () => _showMultiSelect(context),
      child: Container(
        margin: EdgeInsets.only(left: 24, right: 24, bottom: 24),
        child: FormField<String>(
          builder: (FormFieldState<String> state) {
            return InputDecorator(
              decoration: InputDecoration(
                labelStyle: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                errorStyle: TextStyle(color: Colors.red, fontSize: 14.0),
                hintText: StringText.tags,
                hintStyle: TextStyle(color: Colors.grey),
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(8.0),
                  borderSide: new BorderSide(
                    color: Colors.grey,
                  ),
                ),
              ),
              isEmpty: _currentSelectedValue == "",
              child: selectedValues != null
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          selectedValues!.join(", "),
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        Icon(Icons.arrow_drop_down)
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          StringText.selectTags,
                          style: TextStyle(
                              color: Colors.grey, fontWeight: FontWeight.bold),
                        ),
                        Icon(Icons.arrow_drop_down)
                      ],
                    ),
            );
          },
        ),
      ),
    );
  }

  void _showMultiSelect(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (ctx) {
        return MultiSelectDialog(
            items: _movieNewStore!.categories!.categories
                .map((e) => MultiSelectItem(e, e))
                .toList(),
            initialValue: [],
            onConfirm: (values) {
              setState(() {
                selectedValues = values.cast<String>();
              });
            });
      },
    );
  }
}
