import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:movies_app/data/constants/store_stare.dart';
import 'package:movies_app/data/constants/string.dart';
import 'package:movies_app/data/models/movie.dart';
import 'package:movies_app/materials/bottom_sheet.dart';
import 'package:movies_app/routes/router.gr.dart';
import 'package:movies_app/stores/movie_list_store.dart';

class MovieListPage extends StatefulWidget {
  @override
  _MovieListPageState createState() => _MovieListPageState();
}

class _MovieListPageState extends State<MovieListPage> {
  MovieListStore? _movieListStore;
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  List<ReactionDisposer>? _disposers;

  @override
  void initState() {
    _movieListStore ??= MovieListStore();
    _disposers ??= [
      reaction((_) => _movieListStore?.errorMessage, (message) {
        showModalBottomSheet(
          enableDrag: false,
          isDismissible: false,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (context) => BottomSheetMaterial()
              .bottomSheetDialog(message.toString(), context, () {
            Navigator.pop(context);
          }),
        );
      })
    ];
    super.initState();
  }

  @override
  void dispose() {
    _disposers?.forEach((d) => d.call());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _movieListStore?.getMovies();
    return Scaffold(
      key: _scaffoldMessengerKey,
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          StringText.movies,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => AutoRouter.of(context).push(MovieNewPageRoute()),
            child: Container(
              margin: EdgeInsets.only(right: 16),
              child: Text(
                StringText.new_,
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ),
        ],
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Observer(builder: (_) {
          switch (_movieListStore!.state) {
            case StoreState.initial:
              return Container();
            case StoreState.loading:
              return Center(child: CircularProgressIndicator());
            case StoreState.loaded:
              return _moviesView(_movieListStore!.movies!);
          }
        }),
      ),
    );
  }

  Widget _moviesView(List<Movie> movies) {
    return Container(
      margin: EdgeInsets.only(top: 24),
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: movies.length,
          itemBuilder: (context, i) {
            return GestureDetector(
              onTap: () => AutoRouter.of(context)
                  .push(MovieEditPageRoute(movie: movies[i])),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 2,
                        blurRadius: 4,
                        offset: Offset(0, 3),
                      ),
                    ]),
                margin: EdgeInsets.only(
                  left: 24,
                  right: 24,
                  bottom: 24,
                ),
                alignment: Alignment.centerLeft,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          left: 16, top: 16, bottom: 6, right: 16),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        movies[i].title,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16, bottom: 16),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        movies[i].director,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }
}
