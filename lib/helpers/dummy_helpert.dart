import 'package:movies_app/data/models/categories.dart';
import 'package:movies_app/data/models/movie.dart';

class DummyHelper {
  final firstMovie = Movie(
      id: 1,
      title: "Army of the Dead",
      director: "Zack Snyder",
      summary:
          "Following a zombie outbreak in Las Vegas, a group of mercenaries take the ultimate gamble, venturing into the quarantine zone to pull off the greatest heist ever attempted.",
      tags: "Action");
  final secondMovie = Movie(
      id: 2,
      title: "Mr. Bean's Holiday",
      director: "Steve Bendelack",
      summary:
          "Mr. Bean wins a trip to Cannes where he unwittingly separates a young boy from his father and must help the two reunite. On the way he discovers France, bicycling, and true love.",
      tags: "Comedy");
  final thirdMovie = Movie(
      id: 3,
      title: "Wonder Woman",
      director: "Patty Jenkins",
      summary:
          "When a pilot crashes and tells of conflict in the outside world, Diana, an Amazonian warrior in training, leaves home to fight a war, discovering her full powers and true destiny.",
      tags: "Fantasy");
  final fourthMovie = Movie(
      id: 4,
      title: "The Counjuring",
      director: "James Wan",
      summary:
          "Paranormal investigators Ed and Lorraine Warren work to help a family terrorized by a dark presence in their farmhouse.",
      tags: "Horror");
  final fifthMovie = Movie(
      id: 5,
      title: "Ready Player One",
      director: "Steven Spielberg",
      summary:
          "When the creator of a virtual reality called the OASIS dies, he makes a posthumous challenge to all OASIS users to find his Easter Egg, which will give the finder his fortune and control of his world.",
      tags: "Sci-Fi");

  final categories = ["Action", "Comedy", "Fantasy", "Horror", "Sci-Fi"];

  Future<List<Movie>> initMovies() async {
    List<Movie> movies = [];
    movies.add(firstMovie);
    movies.add(secondMovie);
    movies.add(thirdMovie);
    movies.add(fourthMovie);
    movies.add(fifthMovie);

    return movies;
  }

  Future<Categories> initCategories() async {
    List<String> categories = [];
    categories.add("Action");
    categories.add("Comedy");
    categories.add("Fantasy");
    categories.add("Horror");
    categories.add("Sci-Fi");

    final categoryRespons = Categories(categories);
    return categoryRespons;
  }
}
