import 'package:shared_preferences/shared_preferences.dart';

class PrefHelper {
  late SharedPreferences preferences;
  Future<SharedPreferences> _preferences = SharedPreferences.getInstance();

  Future<SharedPreferences> _getPreferences() async {
    preferences = await SharedPreferences.getInstance();;
    return preferences;
  }

  void setString(String key, String value) async {
    await _getPreferences().then((prefs) {
      prefs.setString(key, value);
    });
  }

  Future<String> getString(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final value = prefs.getString(key);
    return value ?? "";
  }
}
