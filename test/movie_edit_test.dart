import 'package:flutter_test/flutter_test.dart';
import 'package:movies_app/data/models/movie.dart';
import 'package:movies_app/stores/movie_edit_store.dart';

main() {
  MovieEditStore? _movieEditStore;

  setUp(() {
    _movieEditStore = MovieEditStore();
  });

  test("updated movie data should not be empty", () async {
    // Arrange
    String title = "Justice Leage";
    String director = "Zack Snyder";
    String summary = "A Superhero Movie";
    String tags = "Action, Fantasy";
    // Act
    final result =
        await _movieEditStore?.movieValidation(title, director, summary, tags);
    // Assert
    expect(result, true);
  });

  test("updated movie data should be empty", () async {
    // Arrange
    String title = "";
    String director = "";
    String summary = "";
    String tags = "";
    // Act
    final result =
        await _movieEditStore?.movieValidation(title, director, summary, tags);
    // Assert
    expect(result, false);
  });

  test("should be return movies to update", () async {
    // Arrange
    List<Movie> movies = [];
    final firstMovie = Movie(
        id: 1,
        title: "Justice League",
        director: "Zack Snyder",
        summary: "A superhero movie",
        tags: "Action, Fantasy");
    final secondMovie = Movie(
        id: 2,
        title: "Tenet",
        director: "Christoper Nolan",
        summary: "A Nolan movie",
        tags: "Action");
    final newMovie = Movie(
        id: 2,
        title: "Wonder Woman",
        director: "Zack Snyder",
        summary: "DC Extended Universe",
        tags: "Action, Fantasy");

    movies.add(firstMovie);
    movies.add(secondMovie);
    // Act
    final result = await _movieEditStore?.updateDataMovies(movies, newMovie);
    // Assert
    expect(result?.isNotEmpty, true);
  });

  test("failed to update movie", () async {
    // Arrange
    List<Movie> movies = [];
    final firstMovie = Movie(
        id: 1,
        title: "Justice League",
        director: "Zack Snyder",
        summary: "A superhero movie",
        tags: "Action, Fantasy");
    final secondMovie = Movie(
        id: 2,
        title: "Tenet",
        director: "Christoper Nolan",
        summary: "A Nolan movie",
        tags: "Action");

    movies.add(firstMovie);
    movies.add(secondMovie);
    // Act
    final result = await _movieEditStore?.updateDataMovies(movies, null);
    // Assert
    expect(result?.isEmpty, true);
  });

  test("should be return true when deleting an existing movie", () async {
    // Arrange
    List<Movie> movies = [];
    final firstMovie = Movie(
        id: 1,
        title: "Justice League",
        director: "Zack Snyder",
        summary: "A superhero movie",
        tags: "Action, Fantasy");
    final secondMovie = Movie(
        id: 2,
        title: "Tenet",
        director: "Christoper Nolan",
        summary: "A Nolan movie",
        tags: "Action");

    movies.add(firstMovie);
    movies.add(secondMovie);
    // Act
    final result = await _movieEditStore?.deleteMovies(movies, 2);
    // Assert
    expect(result?.isNotEmpty, true);
  });

  test("failed to delete movie", () async {
    // Arrange
    List<Movie> movies = [];
    final firstMovie = Movie(
        id: 1,
        title: "Justice League",
        director: "Zack Snyder",
        summary: "A superhero movie",
        tags: "Action, Fantasy");
    final secondMovie = Movie(
        id: 2,
        title: "Tenet",
        director: "Christoper Nolan",
        summary: "A Nolan movie",
        tags: "Action");

    movies.add(firstMovie);
    movies.add(secondMovie);
    // Act
    final result = await _movieEditStore?.deleteMovies(movies, 2000);
    // Assert
    expect(result?.isEmpty, true);
  });
}
