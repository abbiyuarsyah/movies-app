import 'package:flutter_test/flutter_test.dart';
import 'package:movies_app/data/models/movie.dart';
import 'package:movies_app/stores/movie_new_store.dart';

main() {
  MovieNewStore? _movieNewStore;

  setUp(() {
    _movieNewStore = MovieNewStore();
  });

  test("should be return movies to save", () async {
    // Arrange
    List<Movie> movies = [];
    final firstMovie = Movie(
        id: 1,
        title: "Justice League",
        director: "Zack Snyder",
        summary: "A superhero movie",
        tags: "Action, Fantasy");
    final secondMovie = Movie(
        id: 2,
        title: "Tenet",
        director: "Christoper Nolan",
        summary: "A Nolan movie",
        tags: "Action");
    final newMovie = Movie(
        id: 3,
        title: "Wonder Woman",
        director: "Patty Jenkins",
        summary: "DC Extended Univers",
        tags: "Action, Fantasy");

    movies.add(firstMovie);
    movies.add(secondMovie);
    // Act
    final result = await _movieNewStore?.saveDataMovies(movies, newMovie);
    // Assert
    expect(result?.isNotEmpty, true);
  });

  test("failed to save movies", () async {
    // Arrange
    List<Movie> movies = [];
    final firstMovie = Movie(
        id: 1,
        title: "Justice League",
        director: "Zack Snyder",
        summary: "A superhero movie",
        tags: "Action, Fantasy");
    final secondMovie = Movie(
        id: 2,
        title: "Tenet",
        director: "Christoper Nolan",
        summary: "A Nolan movie",
        tags: "Action");

    movies.add(firstMovie);
    movies.add(secondMovie);
    // Act
    final result = await _movieNewStore?.saveDataMovies(movies, null);
    // Assert
    expect(result?.isEmpty, true);
  });

  test("new movie data should not be empty", () async {
    // Arrange
    String title = "Justice Leage";
    String director = "Zack Snyder";
    String summary = "A Superhero Movie";
    String tags = "Action, Fantasy";
    // Act
    final result = await _movieNewStore?.addMovieValidation(
        title, director, summary, tags);
    // Assert
    expect(result, true);
  });

  test("new movie data should  be empty", () async {
    // Arrange
    String title = "";
    String director = "";
    String summary = "";
    String tags = "";
    // Act
    final result = await _movieNewStore?.addMovieValidation(
        title, director, summary, tags);
    // Assert
    expect(result, false);
  });
}
